# Во всех подпапках этой директории все файлы папки .terraform
**/.terraform/*

# Файлы имеющие в имени .tfstate. или файлы с расширением tfstate в этой директории
*.tfstate
*.tfstate.*

# Файл crash.log в этой директории
crash.log

# Файлы с расширением tfvars в этой директории
*.tfvars

# Файлы override.tf override.tf.json в этой директории
override.tf
override.tf.json

# Файлы, заканчивающиеся на _override.tf или _override.tf.json в этой директории
*_override.tf
*_override.tf.json

Это просто комментарий, показывающий как исключить файл из ранее игнорируемых
# !example_override.tf

# Файлы .terraformrc terraform.rc в этой директории
.terraformrc
terraform.rc
